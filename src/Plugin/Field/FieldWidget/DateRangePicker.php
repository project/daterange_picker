<?php

namespace Drupal\daterange_picker\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'daterange_picker' widget.
 *
 * @FieldWidget(
 *   id = "daterange_picker",
 *   module = "daterange_picker",
 *   label = @Translation("Date Range Picker"),
 *   field_types = {
 *     "daterangepicker", 
 *   }
 * )
 */
class DateRangePicker extends WidgetBase {

  /**
   * {@inheritdoc}
   */

  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

   $value = isset($items[$delta]->daterangepicker) ? $items[$delta]->daterangepicker : '';
   $element += [
      '#type' => 'textfield',
      '#default_value' => $value,
      '#attributes' => [
        'class' => ['custom_daterange_picker'],
      ],
	  '#attached' => [
        'library' => [
          'daterange_picker/daterange_picker'
        ],
      ],
    ];


    return ['daterangepicker' => $element];

  }


}
