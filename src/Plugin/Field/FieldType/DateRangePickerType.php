<?php

/**
 * @file
 * Contains \Drupal\daterane_picker\Plugin\Field\FieldType\DateRangePickerType.
 */

namespace Drupal\daterange_picker\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;




/**
 * Plugin implementation of the 'DateRangePicker' field type.
 *
 * @FieldType(
 *   id = "daterangepicker",
 *   label = @Translation("Date Range Picker"),
 *   description = @Translation("This field stores date range in database."),
 *   default_widget = "daterange_picker",
 *   default_formatter = "daterange_picker_formatter"
 * )
 */
class DateRangePickerType extends FieldItemBase { 

	
	/**
	 * {@inheritdoc}
	 */
	public static function schema(FieldStorageDefinitionInterface $field) {
	  return array(
		'columns' => array(
		  'daterangepicker' => array(
			'type' => 'varchar',
			'length' => 256,
			'not null' => FALSE,
		  ),
		),
	  );
	}
	
	
	/**
	 * {@inheritdoc}
	 */
	public function isEmpty() {
	  $value = $this->get('daterangepicker')->getValue();
	  return $value === NULL || $value === '';
	}
	
	
	/**
	 * {@inheritdoc}
	 */
	public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
		
		$properties = [];
		
		$properties['daterangepicker'] = DataDefinition::create('string')
          ->setLabel(t('DateRangePicker'))
          ->setRequired(TRUE);

		 return $properties;

	  }


}