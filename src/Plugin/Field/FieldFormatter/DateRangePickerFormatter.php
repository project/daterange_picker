<?php

namespace Drupal\daterange_picker\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of our "DateRangePicker" formatter.
 *
 * @FieldFormatter(
 *   id = "daterange_picker_formatter",
 *   module = "daterange_picker",
 *   label = @Translation("Date Range Picker formatter"),
 *   field_types = {
 *     "daterangepicker"
 *   }
 * )
 */
class DateRangePickerFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

   

    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        // We wrap the fieldnote content up in a div tag.
        '#type' => 'html_tag',
        '#tag' => 'div',
        // This text is auto-XSS escaped.  See docs for the html_tag element.
        '#value' => $item->daterangepicker,


      ];
    }
	
	return $elements;
  }

}
