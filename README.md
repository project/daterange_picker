# Date Range Picker

Date Range Picker intends to create new Field Type "Date Range Picker".

 - For a full description of the module, visit the project page:
   https://www.drupal.org/project/daterange_picker

 - To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/search/daterange_picker 

## CONTENTS OF THIS FILE

 - Requirements
 - Credit
 - Installation
 - Configuration
 - Maintainers
   

## REQUIREMENTS

 - No special requirements.


## CREDIT

 - Module is using downloaded version of js and css of "moment.min.js, daterangepicker.min.js, daterangepicker.css" Library.  
 
 
 
## INSTALLATION

 - Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


## CONFIGURATION
 - Select Field Type called "Date Range Picker"  
 
## MAINTAINERS
