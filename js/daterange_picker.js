/*
@File is designed to provide date range picker initialization 
*/
(function ($, Drupal) {
  Drupal.behaviors.daterange_picker = {
    attach: function attach(context, settings) {
		

		
         $('.custom_daterange_picker').daterangepicker({
			timePicker: true,

			locale: {
			  format: 'M/DD/YYYY hh:mm A'
			}
		  });
    }
  };

})(jQuery, Drupal);
